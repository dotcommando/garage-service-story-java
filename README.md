# Garage Service Story
**To read**: [https://epa.ms/garage-service-story-java]

**Average reading time**: 40 minutes

## Story Outline
This story contains group of components for public garage software package.
While classes design and code organization looks not so bad, there're many issues with
names of classes, functions, fields and variables which become a problem to read and understand logic.

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #clean_code, #naming