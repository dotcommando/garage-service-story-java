package com.epam.engx.story;

import com.epam.engx.story.exception.FreeGarageIsNotFoundException;
import com.epam.engx.story.model.Car;

public interface GarageService {

    int registerInGarage(Car car) throws FreeGarageIsNotFoundException;

}
