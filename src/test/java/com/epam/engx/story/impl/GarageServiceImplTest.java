package com.epam.engx.story.impl;

import com.epam.engx.story.GarageCleaningService;
import com.epam.engx.story.exception.FreeGarageIsNotFoundException;
import com.epam.engx.story.model.Car;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static com.epam.engx.story.impl.GarageServiceImpl.SECURE_GARAGES;
import static com.epam.engx.story.impl.GarageServiceImpl.SIMPLE_GARAGES;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class GarageServiceImplTest {

    @Mock
    private GarageCleaningService garageCleaningService;

    private GarageServiceImpl garageService;

    @Before
    public void setUp() {
        garageService = new GarageServiceImpl(garageCleaningService);
    }

    @Test
    public void whenCarIsParked_ThenGarageIsCleaned() throws FreeGarageIsNotFoundException {
        // given
        Car simpleCar = new Car("Toyota", "Prius", 2015, false);
        // when
        garageService.registerInGarage(simpleCar);
        // then
        then(garageCleaningService).should().clean(anyInt());
    }

    @Test
    public void whenCarIsClassic_ThenParkedToIncreasedSecurityGarage() throws FreeGarageIsNotFoundException {
        // given
        Car classicCar = new Car("Aston Martin", "DB4", 1958, true);
        // when
        Integer garageWhereCarIsParked = garageService.registerInGarage(classicCar);
        // then
        assertTrue(garageWithinArrayOfGarages(garageWhereCarIsParked, SECURE_GARAGES));
    }


    @Test
    public void whenCarIsNotClassic_ThenParkedToUsualGarage() throws FreeGarageIsNotFoundException {
        // given
        Car simpleCar = new Car("Toyota", "Prius", 2015, false);
        // when
        int garageWhereCarIsParked = garageService.registerInGarage(simpleCar);
        // then
        assertTrue(garageWithinArrayOfGarages(garageWhereCarIsParked, SIMPLE_GARAGES));
    }

    @Test(expected = FreeGarageIsNotFoundException.class)
    public void whenAllGaragesHaveCars_ThenExceptionIsThrown() throws FreeGarageIsNotFoundException {
        // given
        Car simpleCar = new Car("Toyota", "Prius", 2015, false);
        parkCarsInAllGarages();
        // when
        garageService.registerInGarage(simpleCar);
    }

    private void parkCarsInAllGarages() {
        Map<Integer, Car> busyGarages = new HashMap<>();
        for (int increasedSecurityGarage : SECURE_GARAGES) {
            busyGarages.put(increasedSecurityGarage, mock(Car.class));
        }
        for (int usualGarage : SIMPLE_GARAGES) {
            busyGarages.put(usualGarage, mock(Car.class));
        }
        garageService.setGaragesWithCar(busyGarages);
    }


    private boolean garageWithinArrayOfGarages(Integer garageWhereCarIsParked, int[] garages) {
        return Arrays.stream(garages).anyMatch(garageWhereCarIsParked::equals);
    }
}